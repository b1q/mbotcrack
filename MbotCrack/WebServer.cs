﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace MbotCrack
{
    public static class WebServer
    {
        private static HttpListener webServer = null;

        public static void StartServer()
        {
            webServer = new HttpListener();
            webServer.Prefixes.Add("http://127.0.0.1:8055/");
            webServer.Start();
            Accept();
        }

        static void Accept()
        {
            void ACP(IAsyncResult iar)
            {
                var context = webServer.EndGetContext(iar);
                ProcessRequest(context);
                Accept();
            }
            try
            {
                webServer.BeginGetContext(ACP, null);
            }
            catch { }
        }

        static void ProcessRequest(HttpListenerContext context)
        {
            try
            {
                var req = context.Request;
                Console.WriteLine($"HandlingRequest: {req.Url.ToString()}");
                if (req.Url.ToString().Contains("pa/auth.psro.mbot.1.php"))
                {
                    var resp = context.Response;
                    string responseText = "@0.2CEA9DD0D41A2E06FD2F3D628566247BB14E114E594767090FEDAFC3169D92CA2CA3918B2EB4C38E3D8F38822D9754E0238C3DFD4DFF50945096248B398330842BEF2BEF2B84408440842BEF2BEF2B5419A80A09CA1D07.AC69C053AA000000009874780E@";
                    resp.ContentLength64 = responseText.Length;
                    var stream = resp.OutputStream;
                    stream.Write(Encoding.UTF8.GetBytes(responseText), 0, responseText.Length);
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

    }
}
