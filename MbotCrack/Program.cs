﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MbotCrack
{
    class Program
    {
        static void Main(string[] args)
        {
            WebServer.StartServer();
            Console.WriteLine("MbotCrack is Listening on port 8055");

            new Thread(() =>
            {
                while (Console.ReadLine() != null)
                {
                    Thread.Sleep(100);
                }
            }).Start();
        }
    }
}
